/***************************************
Class: CS251
Assignment Number: 2
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef ARRAY_CPP
#define ARRAY_CPP

#include <sys/types.h>
#include <iostream>
#include "Array.h"

#if !defined (__INLINE__)
#define INLINE 
#include "Array.inl"
#endif /* __INLINE__ */

#include <algorithm>
#include <sstream>

template <typename T> 
Array<T>::Array (size_t size)
:   max_size_(size),
    cur_size_(size),
    default_value_(new T(0)),
    array_(new T[size])
{
}

template <typename T> 
Array<T>::Array (size_t size, const T &default_value)
:   max_size_(size),
    cur_size_(size),
    default_value_(new T(default_value)),
    array_(new T[size])
{
    std::fill(array_.get(), array_.get() + size, default_value);
}

// The copy constructor (performs initialization).

template <typename T> 
Array<T>::Array (const Array<T> &s)
:   max_size_(s.max_size_),
    cur_size_(s.cur_size_),
    default_value_(new T (*s.default_value_)),
    array_(new T[s.max_size_])
{
    std::fill(array_.get(), array_.get() + max_size_, *
              default_value_);
    std::copy(s.array_.get(), s.array_.get() + s.cur_size_, array_.get());
}

template <typename T> void
Array<T>::resize (size_t new_size)
{
    Array<T> new_array(new_size);
    new_array.max_size_ = new_size;
    new_array.cur_size_ = cur_size_;
    std::fill(new_array.array_.get(), new_array.array_.get() + new_size, *default_value_);
    std::copy(array_.get(), array_.get() + cur_size_, new_array.array_.get());
    swap(new_array);
}

template <typename T> void
Array<T>::swap (Array<T> &new_array)
{
    std::swap (new_array.cur_size_,  cur_size_);
    std::swap (new_array.max_size_, max_size_);
    
    new_array.array_.swap (array_);
}

// Assignment operator (performs assignment).

template <typename T> Array<T> &
Array<T>::operator= (const Array<T> &s)
{
    if (this == &s) return *this;
    Array<T> base_array(s);
    swap(base_array);
    
    return *this;
}

// Clean up the array (e.g., delete dynamically allocated memory).

template <typename T> 
Array<T>::~Array (void)
{

}

// = Set/get methods.

// Set an item in the array at location index.  

template <typename T> void
Array<T>::set (const T &new_item, size_t index)
{
    if (!in_range(index))
        resize(max_size_ * 2);
    
    if (index >= cur_size_)
        cur_size_ = index + 1;
    
    array_[index] = new_item;
}

// Get an item in the array at location index.

template <typename T> void
Array<T>::get (T &item, size_t index) const
{
    if (!in_range(index))
    {
        std::stringstream x;
        x << (int) index;
        throw std::out_of_range(x.str());
    }
    item = array_[index];
}

// Compare this array with <s> for equality.

template <typename T> bool
Array<T>::operator== (const Array<T> &s) const
{
    bool isEqual = false;
    if (this->cur_size_ == s.cur_size_ && this->max_size_ == s.max_size_)
    {
        isEqual = std::equal(array_.get(), array_.get() + cur_size_, s.array_.get());
    }
    return isEqual;
}

// Compare this array with <s> for inequality.

template <typename T> bool
Array<T>::operator!= (const Array<T> &s) const
{
    return !(*this == s);
}

#endif /* ARRAY_CPP */
