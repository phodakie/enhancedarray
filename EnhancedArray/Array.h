/***************************************
Class: CS251
Assignment Number: 2
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

/* -*- C++ -*- */

#ifndef ARRAY_H
#define ARRAY_H

// This header defines "size_t"
#include <stdlib.h>
#include <stdexcept>
#include <memory>
#include "scoped_array.h"

/**
 * @class Array
 * @brief Implements a vector that resizes.
 */
template <typename T>
class Array
{
public:
  // Define a "trait"
  typedef T value_type;

  // = Initialization and termination methods.

  // Dynamically create an uninitialized array.  Throws <std::bad_alloc>
  // if allocation fails.
  Array (size_t size);

  // Dynamically initialize the entire array to the <default_value>.
  // Throws <std::bad_alloc> if allocation fails.
  Array (size_t size, const T &default_value);

  // The copy constructor performs initialization by making an exact
  // copy of the contents of parameter <s>, i.e., *this == s will
  // return true.  Throws <std::bad_alloc> if allocation fails.
  Array (const Array<T> &s);

  // Assignment operator performs an assignment by making a copy of
  // the contents of parameter <s>, i.e., *this == s will return true.
  // Throws <std::bad_alloc> if allocation fails.  If any exceptions
  // occur, however, the state of the original array remains unchanged
  // (i.e., implement "strong exception guarantee" semantics).
  Array<T> &operator= (const Array<T> &s);

  // Clean up the array (e.g., delete dynamically allocated memory).
  ~Array (void);

  // = Set/get methods.

  // Set an item in the array at location index.  If <index> >=
  // <s.cur_size_> then <resize()> the array so it's big enough.
  // Throws <std::bad_alloc> if resizing the array fails.
  void set (const T &new_item, size_t index);

  // Get an item in the array at location index.  Throws <std::out_of_range>
  // of index is not <in_range>.
  void get (T &item, size_t index) const;

  // Returns the <cur_size_> of the array.
  size_t size (void) const;

  // Returns a reference to the <index> element in the <Array> without
  // checking for range errors.
  const T &operator[] (size_t index) const;

  // Set an item in the array at location index without 
  // checking for range errors.
  T &operator[] (size_t index);

  // Compare this array with <s> for equality.  Returns true if the
  // size()'s of the two arrays are equal and all the elements from 0
  // .. size() are equal, else false.
  bool operator== (const Array<T> &s) const;

  // Compare this array with <s> for inequality such that <*this> !=
  // <s> is always the complement of the boolean return value of
  // <*this> == <s>.
  bool operator!= (const Array<T> &s) const;

  // Change the size of the array to be at least <new_size> elements.
  // Throws <std::bad_alloc> if allocation fails.
  void resize (size_t new_size);

  // Efficiently swap the contents of this <Array> with <new_array>.
  // Does not throw an exception.
  void swap (Array<T> &new_array);

private:
  // Returns true if <index> is within range, i.e., 0 <= <index> <
  // <cur_size_>, else returns false.
  bool in_range (size_t index) const;

  // Maximum size of the array, i.e., the total number of <T> elements
  // in <array_>.
  size_t max_size_;
        
  // Current size of the array.  This starts out being == to
  // <max_size_>.  However, if we are assigned a smaller array, then
  // <cur_size_> will become less than <max_size_>.  The purpose of
  // keeping track of both sizes is to avoid reallocating memory if we
  // don't have to.
  size_t cur_size_;

  // If a default value is needed, keep track of its value.
  std::unique_ptr<T> default_value_;

  // @@ Replace unique_ptr with auto_ptr if your compiler doesn't support C++11.
  //std::auto_ptr<T> default_value_;

  // Pointer to the array's storage buffer.
  scoped_array<T> array_;
};

#if defined (__INLINE__)
#define INLINE inline
#include "Array.inl"
#endif /* __INLINE__ */

#include "Array.cpp"

#endif /* ARRAY_H */
